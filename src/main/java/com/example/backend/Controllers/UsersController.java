package com.example.backend.Controllers;

import com.example.backend.Entity.Users;
import com.example.backend.Services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.Set;

@RestController
@RequestMapping("/private/user")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @RequestMapping(value = "/fr", method = RequestMethod.GET, produces = "application/json")
    public Set<Users> getUserFriends(Principal user){
        return  usersService.getUserFriends(user.getName());
    }
}

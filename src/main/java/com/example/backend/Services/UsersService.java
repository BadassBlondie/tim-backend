package com.example.backend.Services;

import com.example.backend.Entity.Users;
import com.example.backend.Repos.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UsersService {

    @Autowired
    private UsersRepo usersRepo;

    public Set<Users> getUserFriends(String name) {
        Users user = usersRepo.findUsersByUserName(name);
        return user.getFriends();
    }
}

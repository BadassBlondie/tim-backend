package com.example.backend.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity(name =  "image")
@Table(name = "image")
public class Image implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "image_Id", updatable = false, nullable = false)
    private Integer imageId;

    private Integer imageFrameNumber;
    private String imageFrameFileName;
    private String imageFramePath;

    @OneToMany(mappedBy = "image")
    private Set<Comments> comments = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "posts_Id")
    @JsonBackReference
    private Posts posts;

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public Integer getImageFrameNumber() {
        return imageFrameNumber;
    }

    public void setImageFrameNumber(Integer imageFrameNumber) {
        this.imageFrameNumber = imageFrameNumber;
    }

    public String getImageFrameFileName() {
        return imageFrameFileName;
    }

    public void setImageFrameFileName(String imageFrameFileName) {
        this.imageFrameFileName = imageFrameFileName;
    }

    public String getImageFramePath() {
        return imageFramePath;
    }

    public void setImageFramePath(String imageFramePath) {
        this.imageFramePath = imageFramePath;
    }

    public Set<Comments> getComments() {
        return comments;
    }

    public void setComments(Set<Comments> comments) {
        this.comments = comments;
    }

    public Posts getPosts() {
        return posts;
    }

    public void setPosts(Posts posts) {
        this.posts = posts;
    }
}

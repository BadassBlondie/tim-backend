package com.example.backend.Entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity(name =  "posts")
@Table(name = "posts")
public class Posts implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "posts_Id", updatable = false, nullable = false)
    private Integer postsId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_Id")
    @JsonBackReference
    private Users users;

    @OneToMany(mappedBy = "posts")
    private Set<Image> images = new HashSet<>();

    @OneToMany(mappedBy = "posts")
    private Set<PostAuth> postAuths = new HashSet<>();

    public Integer getPostsId() {
        return postsId;
    }

    public void setPostsId(Integer postsId) {
        this.postsId = postsId;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Set<Image> getImages() {
        return images;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }

    public Set<PostAuth> getPostAuths() {
        return postAuths;
    }

    public void setPostAuths(Set<PostAuth> postAuths) {
        this.postAuths = postAuths;
    }
}

package com.example.backend.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity(name =  "users")
@Table(name = "users")
@JsonIgnoreProperties({"friends", "friendOf","userPassword", "comments"})
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_Id",updatable = false, nullable = false)
    private Integer userId;

    private String userName;
    private String userFirstName;
    private String userLastName;
    private String userPassword;
    private String userRole;
    private String userRegDate;

    @JsonIgnoreProperties({"friends","friendOf"})
    @ManyToMany
    @JoinTable(name="friends",
            joinColumns=@JoinColumn(name="user_Id"),
            inverseJoinColumns=@JoinColumn(name="friend_Id")
    )
    private Set<Users> friends;

    @JsonIgnoreProperties({"friends","friendOf"})
    @ManyToMany(mappedBy = "friends")
    private Set<Users> friendOf;

    @OneToMany(mappedBy = "users")
    private Set<Comments> comments = new HashSet<>();

    @OneToMany(mappedBy = "users")
    private Set<Posts> posts = new HashSet<>();


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserRegDate() {
        return userRegDate;
    }

    public void setUserRegDate(String userRegDate) {
        this.userRegDate = userRegDate;
    }

    public Set<Users> getFriends() {
        return friends;
    }

    public void setFriends(Set<Users> friends) {
        this.friends = friends;
    }

    public Set<Users> getFriendOf() {
        return friendOf;
    }

    public void setFriendOf(Set<Users> friendOf) {
        this.friendOf = friendOf;
    }

    public Set<Comments> getComments() {
        return comments;
    }

    public void setComments(Set<Comments> comments) {
        this.comments = comments;
    }

    public Set<Posts> getPosts() {
        return posts;
    }

    public void setPosts(Set<Posts> posts) {
        this.posts = posts;
    }
}

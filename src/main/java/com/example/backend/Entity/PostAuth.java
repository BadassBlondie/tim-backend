package com.example.backend.Entity;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity(name = "postAuth")
@Table(name = "post_Auth")
public class PostAuth {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_Auth_Id", updatable = false, nullable = false)
    private Integer postAuthId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "posts_Id")
    @JsonBackReference
    private Posts posts;

    public Integer getPostAuthId() {
        return postAuthId;
    }

    public void setPostAuthId(Integer postAuthId) {
        this.postAuthId = postAuthId;
    }

    public Posts getPosts() {
        return posts;
    }

    public void setPosts(Posts posts) {
        this.posts = posts;
    }
}

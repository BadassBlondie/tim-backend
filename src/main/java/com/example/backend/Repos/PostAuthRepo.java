package com.example.backend.Repos;

import com.example.backend.Entity.PostAuth;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostAuthRepo extends CrudRepository<PostAuth, Long> {
}

package com.example.backend.Repos;

import com.example.backend.Entity.Comments;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentsRepo extends CrudRepository<Comments, Long> {
}

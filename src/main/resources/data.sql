SET IDENTITY_INSERT Users ON
insert into Users (User_Id, user_first_name, user_last_name, User_Name, User_Password, User_Role)values (1, 'User', 'Test', 'user', '$2a$10$9jDdMnNnKvHFkwKnbjBsF.j62ccgMNwVByZ.V4u5PQxTQ665ciRHC', 'USER')
insert into Users (User_Id, user_first_name, user_last_name, User_Name, User_Password, User_Role) values (2, 'Hope', 'User', 'hope','$2a$10$9jDdMnNnKvHFkwKnbjBsF.j62ccgMNwVByZ.V4u5PQxTQ665ciRHC','USER')
SET IDENTITY_INSERT Users OFF
